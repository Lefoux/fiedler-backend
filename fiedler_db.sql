-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 17 oct. 2018 à 16:26
-- Version du serveur :  10.1.30-MariaDB
-- Version de PHP :  7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `fiedler_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `date_work` date NOT NULL,
  `heure_arrivee` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `heure_depart` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `worker_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL,
  `deleted` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `attendance`
--

INSERT INTO `attendance` (`id`, `date_work`, `heure_arrivee`, `heure_depart`, `worker_id`, `status`, `created`, `updated`, `deleted`) VALUES
(1, '2018-10-13', '2018-10-13 17:29:09', '2018-10-13 20:01:37', 1, 1, '2018-10-13 17:29:09', NULL, NULL),
(2, '2018-10-14', '2018-10-14 00:41:15', '2018-10-14 08:40:08', 1, 1, '2018-10-14 00:41:15', NULL, NULL),
(3, '2018-10-16', '2018-10-16 10:34:14', '2018-10-16 10:34:25', 1, 1, '2018-10-16 10:34:14', NULL, NULL),
(4, '2018-10-17', '2018-10-17 09:18:51', '2018-10-17 15:38:57', 2, 1, '2018-10-17 09:18:51', NULL, NULL),
(5, '2018-10-17', '2018-10-17 09:18:52', '2018-10-17 17:39:09', 3, 1, '2018-10-17 09:18:52', NULL, NULL),
(6, '2018-10-17', '2018-10-17 09:19:45', '2018-10-17 14:25:26', 4, 1, '2018-10-17 09:19:45', NULL, NULL),
(7, '2018-10-17', '2018-10-17 09:19:59', '2018-10-17 09:39:14', 1, 1, '2018-10-17 09:19:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`) VALUES
(1, 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 'Admin Administrator');

-- --------------------------------------------------------

--
-- Structure de la table `workers`
--

CREATE TABLE `workers` (
  `id` int(11) NOT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `sexe` char(1) DEFAULT NULL,
  `datenaiss` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `workers`
--

INSERT INTO `workers` (`id`, `lastname`, `firstname`, `sexe`, `datenaiss`, `phone`, `email`, `status`, `created`, `updated`, `deleted`) VALUES
(1, 'ANATHOLIE', 'Vladmir', 'M', '2018-07-18', '92965857', 'dindanelare@gmail.com', 1, '2018-10-13 09:21:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Jack', 'Mesrime', 'F', '1999-12-2', '97124911', 'a@a.com', 1, '2018-10-17 08:43:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Sersei', 'Lanister', 'M', '2000-12-2', '98124911', 'b@b.com', 1, '2018-10-17 08:47:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Kirk', 'Donald', 'F', '2004-6-8', '99124911', 'c@c.com', 1, '2018-10-17 08:56:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `workers`
--
ALTER TABLE `workers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
