<?php
session_start();
require_once 'class/user.class.php';

$path_dir_auth = 'includes/auth.php';
$user = new USER();

if(!$user->is_logged_in())
{
	$user->redirect('../../index.php');
}

if($user->is_logged_in()!="")
{
	$user->logout();	
	$user->redirect('login.php');
}
?>