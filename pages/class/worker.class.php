<?php

if (isset($_POST['add-wrk'])) {
	require_once '../db/dbconf.php';
} else {
	require_once 'db/dbconf.php';
}

class WORKER
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function lasdID()
	{
		$stmt = $this->conn->lastInsertId();
		return $stmt;
	}
	
	public function add($datas = array())
	{
		try
		{							
			$stmt = $this->conn->prepare("INSERT INTO workers (lastname,firstname,sexe,datenaiss,phone,email) VALUES(:lastname,:firstname,:sexe,:datenaiss,:phone,:email)");
			$rep = $stmt->execute($datas);
			return $rep;
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function update($id)
	{
		try
		{
			
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}

}

$theWorker = new WORKER();

if (isset($_POST['add-wrk'])) {
	$nWrk = array(
		':lastname' => $_POST['lastname'],
		':firstname' => $_POST['firstname'],
		':sexe' => $_POST['sexe'],
		':datenaiss' => $_POST['datenaiss'],
		':phone' => $_POST['phone'],
		':email' => $_POST['email']
	 );
	$res = $theWorker->add($nWrk);

	if ($res) {
		$theWorker->redirect('../add.php?success');
	} else {
		$theWorker->redirect('../add.php?error');
	}
}