<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="home.php"><i class="fa fa-dashboard fa-fw"></i> Tableau de Bord</a>
            </li>
            <li>
                <a href="list.php"><i class="fa fa-table fa-fw"></i> Liste des Employers</a>
            </li>
            <li>
                <a href="add.php"><i class="fa fa-edit fa-fw"></i> Ajouter un Employer</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i> Paramètres</a>
            </li>
            
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->