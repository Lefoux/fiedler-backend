<?php
session_start();
require_once 'class/user.class.php';

$user_login = new USER();

if($user_login->is_logged_in()!="")
{
	$user_login->redirect('home.php');
} else {
	$user_login->redirect('../index.php');
}

if(isset($_POST['btn-login']))
{
	$email = trim($_POST['txtemail']);
	$upass = trim($_POST['txtpassword']);
	
	if($user_login->login($email,$upass))
	{
		$user_login->redirect('home.php');
	}
}

?>