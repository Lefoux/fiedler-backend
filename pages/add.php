<?php
session_start();
require_once 'class/user.class.php';
$user_home = new USER();

if(!$user_home->is_logged_in())
{
    $user_home->redirect('login.php');
}

$stmt = $user_home->runQuery("SELECT * FROM users WHERE id=:uid");
$stmt->execute(array(":uid"=>$_SESSION['userSID']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Ajouter</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php include('layouts/head.php'); ?>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">FIEDLER AMS</a>
            </div>
            <!-- /.navbar-header -->

            <!-- Top Bar Nav -->
                <?php include('layouts/topbar-nav.php'); ?>
            <!-- End Top Bar Nav -->


            <!-- Left Bar Nav -->
                <?php include('layouts/leftbar-nav.php'); ?>
            <!-- End Left Bar Nav -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Ajouter Employer</h1>


                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Ajouter
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php 
                                        if (isset($_GET['success'])) {
                                            echo'<div class="alert alert-success alert-dismissable" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <strong>Succès!</strong> Employer enregistré avec succès.
                                                </div>';
                                        } elseif(isset($_GET['error'])) {
                                            echo'<div class="alert alert-danger alert-dismissable" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <strong>Echec!</strong> Erreur enregistrement.
                                                </div>';
                                        }
                                    ?>
                                    <form role="form" method="POST" action="class/worker.class.php" enctype="multipart/form-data">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Nom</label>
                                                <input class="form-control" name="lastname" placeholder="Nom">
                                            </div>
                                            <div class="form-group">
                                                <label>Prénom</label>
                                                <input class="form-control" name="firstname" placeholder="Prénom">
                                            </div>
                                            <div class="form-group">
                                                <label>Sexe</label>
                                                <select name="sexe" class="form-control">
                                                    <option value="F">F</option>
                                                    <option value="M">M</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Date de Naissance</label>
                                                <input class="form-control" name="datenaiss" placeholder="0000-00-00">
                                            </div>
                                            <div class="form-group">
                                                <label>Téléphone</label>
                                                <input class="form-control" name="phone" placeholder="Téléphone">
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input class="form-control" type="email" name="email" placeholder="Email">
                                            </div>
                                            <!--div class="form-group">
                                                <label>Image</label>
                                                <input type="file" name="post-img">
                                            </div-->
                                            <!--div class="form-group">
                                                <label>Publier</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">Publier ?
                                                    </label>
                                                </div>
                                            </div-->
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                        <div class="col-lg-12">
                                            <!--div class="form-group">
                                                <label>Contenu</label>
                                                <textarea name="post-content" class="form-control" rows="10"></textarea>
                                            </div-->
                                            <button type="submit" name="add-wrk" class="btn btn-primary">Valider</button>
                                            <button type="reset" class="btn btn-default">Annuler</button>
                                        </div>
                                        <!-- /.col-lg-12 (nested) -->
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scripts -->
        <?php include('layouts/scripts.php'); ?>
    <!-- End Scripts -->
</body>

</html>
