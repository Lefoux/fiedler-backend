<?php
session_start();
require_once 'class/user.class.php';
require_once 'class/worker.class.php';
$user_home = new USER();
$worker = new WORKER();

if(!$user_home->is_logged_in())
{
    $user_home->redirect('login.php');
}

$stmt = $user_home->runQuery("SELECT * FROM users WHERE id=:uid");
$stmt->execute(array(":uid"=>$_SESSION['userSID']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$stemps = $worker->runQuery("SELECT * FROM workers WHERE status = 1");
$stemps->execute();
$emps = $stemps->fetchAll();

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Employers</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include('layouts/head.php'); ?>

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">FIEDLER AMS</a>
            </div>
            <!-- /.navbar-header -->

            <!-- Top Bar Nav -->
                <?php include('layouts/topbar-nav.php'); ?>
            <!-- End Top Bar Nav -->


            <!-- Left Bar Nav -->
                <?php include('layouts/leftbar-nav.php'); ?>
            <!-- End Left Bar Nav -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Liste Employers</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                               Employers
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Téléphone</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($emps as $key => $emp) {
                                                echo '<tr class="gradeA">
                                                        <td>'.$emp['lastname'].'</td>
                                                        <td>'.$emp['firstname'].'</td>
                                                        <td>'.$emp['phone'].'</td>
                                                        <td>'.$emp['email'].'</td>
                                                        <td class="center"><a class="voir label label-info"  data-name="'.$emp['lastname'].' '.$emp['firstname'].'" data-id="'.$emp['id'].'" data-toggle="modal">Voir</a></td>
                                                    </tr>';
                                            }
                                        ?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->
                                
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Nombre de Présences de : <span id="wname" class="text-info"></span></h4>
                    </div>
                    <div class="modal-body">
                        <form id="form-pres">
                            <div class="form-group">
                                <label>Mois</label>
                                <select class="form-control" name="month" id="month" required="required">
                                    <option selected="selected" value="">Choisir Mois</option>
                                    <option value="1">Janvier</option>
                                    <option value="2">Février</option>
                                    <option value="3">Mars</option>
                                    <option value="4">Avril</option>
                                    <option value="5">Mai</option>
                                    <option value="6">Juin</option>
                                    <option value="7">Juillet</option>
                                    <option value="8">Août</option>
                                    <option value="9">Septembre</option>
                                    <option value="10">Octobre</option>
                                    <option value="11">Novembre</option>
                                    <option value="12">Décembre</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Année</label>
                                <input type="number" min="1999" max="<?php echo date('Y'); ?>" name="year" id="year" class="form-control" required="required">
                            </div>
                            <input type="hidden" name="wid" id="wid" value="">

                        <div>
                            <h3><label>Nombre de présences : </label>
                            <label class="label label-success" id="nbrHeures">0 <span>Heures</span></label></h3>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" id="btn-pres" data-id="0" class="btn btn-primary">Envoyer</button>
                    </div>
                    </form>
                </div>
                <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Scripts -->
        <?php include('layouts/scripts.php'); ?>
        <!-- DataTables JavaScript -->
        <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
        <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                    responsive: true
                });
            });

            $('a.voir').on('click', function(el){
                theId = $(this).attr('data-id');
                theName = $(this).attr('data-name');
                
                $('#wname').text(theName);
                $('#wid').val(theId);
                $('#myModal').modal();
            });

            $('#form-pres').submit(function (e) {
                var wid = $('#wid').val();
                var mois = $('#month').val();
                var annee = $('#year').val();

                if (mois !== '' && annee !== '') {
                    $.post('class/attendance.class.php', {wid : wid, month : mois, year : annee}, function(result) {
                        //alert(result);
                        $('#nbrHeures').html(result + ' Heures');
                    });
                }
                return false;
                //e.preventDefault();
            })
        </script>
    <!-- End Scripts -->
</body>

</html>
